public class Paladin extends Knight {
	
	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
	}
	
	public double getCombatScore() {
		int n = 1;
		int a = 0;
		int b = 1;
		int temp = 0;
		while(b < this.getBaseHp())
		{
			temp = a;
			a = b;
			b = temp + b;
			n++;
		}
		if(n > 2 && b == this.getBaseHp())
			return (double) 1000 + n;
		else
			return (double) this.getBaseHp() * 3;
	}
}