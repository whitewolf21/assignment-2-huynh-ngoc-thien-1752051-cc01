public class Knight extends Fighter {
   
	public Knight(int baseHp, int wp) {
		super(baseHp, wp);
	}
	
	public double getCombatScore() {
		if(Utility.isSquare(Battle.GROUND))
			return (double) this.getBaseHp() * 2;
		else
		{
			if (this.getWp() == 1)
				return (double) this.getBaseHp();
			else
				return (double) this.getBaseHp() / 10;
		}
	}
}